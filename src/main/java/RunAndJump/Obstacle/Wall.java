package RunAndJump.Obstacle;

import RunAndJump.Participant.Player;

public class Wall implements Obstacle {
    private final String name;
    private final int height;

    public Wall(String name, int height) {
        this.name = name;
        this.height = height;
    }

    @Override
    public void overcome(Player p) {
        if (p.getJumpHeight() > height) {
            System.out.println(p.getName() + " " + p.jump() + " the " + name + " with " + height + " meters height");
            System.out.println("===========================================================================");
        } else {
            System.out.println(p.getName() + " CAN'T " + p.jump() + " the " + name + ". Was able to pass " + p.getJumpHeight() + " meter(s) only...");
            System.out.println("===========================================================================");
        }
    }

    @Override
    public int getSize() {
        return height;
    }

    @Override
    public String toString() {
        return "Wall{" +
                "name='" + name + '\'' +
                ", height=" + height +
                '}';
    }
}
