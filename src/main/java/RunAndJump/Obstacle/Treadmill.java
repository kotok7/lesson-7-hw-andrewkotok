package RunAndJump.Obstacle;

import RunAndJump.Participant.Player;

public class Treadmill implements Obstacle {
    private final String name;
    private final int length;

    public Treadmill(String name, int length) {
        this.name = name;
        this.length = length;
    }

    @Override
    public void overcome(Player p) {
        if (p.getRunLength() > length) {
            System.out.println(p.getName() + " " + p.run() + " the " + name + " with " + length + " meters length");
            System.out.println("===========================================================================");
        } else {
            System.out.println(p.getName() + " CAN'T " + p.run() + " the " + name + ". Was able to pass " + p.getRunLength() + " meter(s) only...");
            System.out.println("===========================================================================");
        }
    }

    @Override
    public int getSize() {
        return length;
    }

    @Override
    public String toString() {
        return "Treadmill{" +
                "name='" + name + '\'' +
                ", length=" + length +
                '}';
    }
}
