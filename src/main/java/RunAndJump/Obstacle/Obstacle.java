package RunAndJump.Obstacle;

import RunAndJump.Participant.Player;

public interface Obstacle {
    void overcome(Player p);
    int getSize();
}
