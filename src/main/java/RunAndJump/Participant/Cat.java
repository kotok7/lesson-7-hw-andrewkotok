package RunAndJump.Participant;


public class Cat extends Player {
    public Cat(String name, int jumpHeight, int runLength) {
        super(name, jumpHeight, runLength);
    }

    @Override
    public String jump() {
        return "JUMPED OVER";
    }

    @Override
    public String run() {
        return "RAN THROUGH";
    }
}
