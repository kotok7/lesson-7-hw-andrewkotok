package RunAndJump.Participant;

public abstract class Player {
    private final String name;
    private final int jumpHeight;
    private final int runLength;

    public Player(String name, int jumpHeight, int runLenght) {
        this.name = name;
        this.jumpHeight = jumpHeight;
        this.runLength = runLenght;
    }

    public String getName() {
        return name;
    }

    public int getJumpHeight() {
        return jumpHeight;
    }

    public int getRunLength() {
        return runLength;
    }

    public String jump() {
        String j = "JUMPED OVER";
        return j;
    }

    public String run() {
        String r = "RAN THROUGH";
        return r;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", jumpHeight=" + jumpHeight +
                ", runLenght=" + runLength +
                '}';
    }
}
