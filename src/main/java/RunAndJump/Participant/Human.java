package RunAndJump.Participant;

public class Human extends Player {
    public Human(String name, int jumpHeight, int runLength) {
        super(name, jumpHeight, runLength);
    }

    @Override
    public String jump() {
        return "JUMPED OVER";
    }

    @Override
    public String run() {
        return "RAN THROUGH";
    }
}
