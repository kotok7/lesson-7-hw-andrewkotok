package RunAndJump;

import RunAndJump.Obstacle.Obstacle;
import RunAndJump.Obstacle.Treadmill;
import RunAndJump.Obstacle.Wall;
import RunAndJump.Participant.Cat;
import RunAndJump.Participant.Human;
import RunAndJump.Participant.Player;
import RunAndJump.Participant.Robot;

public class Main {
    public static void main(String[] args) {

        Player[] players = new Player[]{
                new Cat("Cat", 1, 15),
                new Human("Human", 2, 10),
                new Robot("Robot", 10, 30)
        };

        Obstacle[] obstacles = new Obstacle[]{
                new Wall("Wall", 1),
                new Treadmill("Treadmill", 12),
        };

        interaction(players, obstacles);
    }

    private static void interaction(Player[] players, Obstacle[] obstacles) {
        for (int i = 0; i < players.length; i++) {
            obstacles[0].overcome(players[i]);
            for (int j = 1; j < obstacles.length; j++) {
                if (players[i].getJumpHeight() > obstacles[0].getSize()) {
                    obstacles[j].overcome(players[i]);
                }
            }
        }
    }
}
