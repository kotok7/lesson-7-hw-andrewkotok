package Geometry;

import java.util.Scanner;

public class Triangle implements Squareable{
    private final double a;
    private final double b;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    private final double c;

    @Override
    public double getSquare() {

        double p = (a + b + c ) / 2;
        double square = Math.sqrt(p*(p-a)*(p-b)*(p-c));
        return square;
    }


}
