package Geometry;

import java.util.Scanner;

public class Square implements Squareable{
    private final int a;

    public Square(int a) {
        this.a = a;
    }

    @Override
    public double getSquare() {
        double square = a*a;
        return square;
    }

}
