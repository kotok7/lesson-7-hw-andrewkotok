package Geometry;

import java.util.Scanner;

public class Circle implements Squareable{
    private final double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double getSquare() {
        double square = Math.PI * radius*radius;
        return square;
    }



}
