package Geometry;

public interface Squareable {

    double getSquare();
}
