package Geometry;

public class Main {
    public static void main(String[] args) {
        Squareable[] figures = new Squareable[]{
                new Circle(5),
                new Triangle(5,5,5),
                new Square(5)
        };
        getSum(figures);
    }

    private static void getSum(Squareable[] figure) {
        double sum = 0;
        for (int i = 0; i < figure.length; i++) {
            sum += figure[i].getSquare();
        }
        System.out.printf("%.2f", sum);
    }
}
